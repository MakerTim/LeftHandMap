package nl.makertim.bukkit.lefthandmap;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

public class Mover {

	static public void exportResource(String resourceName, File destination) {
		InputStream stream = null;
		OutputStream resStreamOut = null;
		try {
			stream = Mover.class.getClassLoader().getResourceAsStream(resourceName);
			if (stream == null) {
				throw new Exception("Cannot get resource \"" + resourceName + "\" from Jar file.");
			}

			int readBytes;
			byte[] buffer = new byte[4096];
			resStreamOut = new FileOutputStream(destination);
			while ((readBytes = stream.read(buffer)) > 0) {
				resStreamOut.write(buffer, 0, readBytes);
			}
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		} finally {
			try {
				assert stream != null;
				stream.close();
			} catch (Exception ex) {
				System.err.println("mover:finally:1" + ex);
			}
			try {
				assert resStreamOut != null;
				resStreamOut.close();
			} catch (Exception ex) {
				System.err.println("mover:finally:2" + ex);
			}
		}
	}
}
