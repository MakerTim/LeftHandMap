package nl.makertim.bukkit.lefthandmap;

import java.awt.image.BufferedImage;
import java.io.File;
import java.util.function.BiFunction;

public interface IChunkRenderer extends BiFunction<ChunkSnapshotArray, File, BufferedImage> {

	int CHUNK_SIZE = 16;
	int REGION_SIZE = 32;

	BufferedImage render(ChunkSnapshotArray chunk, File file);

	@Override
	default BufferedImage apply(ChunkSnapshotArray chunk, File file) {
		return render(chunk, file);
	}
}
