package nl.makertim.bukkit.lefthandmap;

import org.bukkit.ChunkSnapshot;
import org.bukkit.Material;
import org.bukkit.block.Biome;
import org.bukkit.block.data.BlockData;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;

public class ChunkRenderer implements IChunkRenderer {

	public static final int PIXEL = 1;

	@Override
	public BufferedImage render(ChunkSnapshotArray chunkArray, File file) {
		BufferedImage image = getImage(file);

		chunkArray.getList().forEach(chunk -> renderChunk(image, chunk));
		return image;
	}

	private BufferedImage getImage(File file) {
		BufferedImage image;
		if (file.exists()) {
			try {
				image = ImageIO.read(file);
			} catch (Exception ex) {
				System.err.println("Failed to load " + file.getAbsolutePath() + " - resetting chunk " + ex);
				image = new BufferedImage(IChunkRenderer.REGION_SIZE * IChunkRenderer.CHUNK_SIZE * PIXEL, IChunkRenderer.REGION_SIZE * IChunkRenderer.CHUNK_SIZE * PIXEL, BufferedImage.TYPE_INT_ARGB);
			}
		} else {
			image = new BufferedImage(IChunkRenderer.REGION_SIZE * IChunkRenderer.CHUNK_SIZE * PIXEL, IChunkRenderer.REGION_SIZE * IChunkRenderer.CHUNK_SIZE * PIXEL, BufferedImage.TYPE_INT_ARGB);
		}

		return image;
	}

	private void renderChunk(BufferedImage image, ChunkSnapshot chunk) {
		int offsetX = chunk.getX() % IChunkRenderer.REGION_SIZE;
		int offsetZ = chunk.getZ() % IChunkRenderer.REGION_SIZE;

		while (offsetX < 0) {
			offsetX += 32;
		}
		while (offsetZ < 0) {
			offsetZ += 32;
		}

		Graphics2D g2d = image.createGraphics();
		g2d.setColor(Color.red);
		for (int relativeX = 0; relativeX < IChunkRenderer.CHUNK_SIZE; relativeX++) {
			for (int relativeZ = 0; relativeZ < IChunkRenderer.CHUNK_SIZE; relativeZ++) {
				String color = null;
				Biome biome = null;
				BlockData block = null;
				for (int y = chunk.getHighestBlockYAt(relativeX, relativeZ); y > 0; y--) {
					block = chunk.getBlockData(relativeX, y, relativeZ);
					if (block.getMaterial() != Material.AIR) {
						Material material = block.getMaterial();
						color = LeftHandMapPlugin.instance.config.colorMap.get(material.toString());
						biome = chunk.getBiome(relativeX, y, relativeZ);
						if (color != null) {
							break;
						}
					}
				}
				drawPixel(color, block, biome, offsetX * IChunkRenderer.CHUNK_SIZE + relativeX, offsetZ * IChunkRenderer.CHUNK_SIZE + relativeZ, g2d);
			}
		}
		g2d.dispose();
	}

	private void drawPixel(String pixelColor, BlockData block, Biome biome, int x, int z, Graphics2D g2d) {
		if (pixelColor == null) {
			pixelColor = "0, 0, 0";
		}

		Color color = colorFromString(pixelColor);
		if (pixelColor.endsWith("*")) {
			String biomeString =
					(block.getMaterial() == Material.WATER ?
							LeftHandMapPlugin.instance.config.waterMap :
							LeftHandMapPlugin.instance.config.biomeMap
					).get(biome.name());
			if (biomeString != null) {
				Color biomeColor = colorFromString(biomeString);
				color = combineColor(color, 1, biomeColor, 1);
			}
		}

		g2d.setColor(color);
		g2d.drawRect(x * PIXEL, z * PIXEL, PIXEL, PIXEL);
	}

	private Color combineColor(Color colorA, int colorAWeight, Color colorB, int colorBWeight) {
		double colorMix = (double) colorBWeight / (double) (colorAWeight + colorBWeight);
		return new Color(
				calcColor(colorA.getRed(), colorB.getRed(), colorMix),
				calcColor(colorA.getGreen(), colorB.getGreen(), colorMix),
				calcColor(colorA.getBlue(), colorB.getBlue(), colorMix)
		);
	}

	private int calcColor(int colorA, int colorB, double colorMix) {
		//sqrt((1 - t) * a^2 + t * b^2)
		return (int) Math.sqrt(((1D - colorMix) * Math.pow(colorA, 2)) + (colorMix * Math.pow(colorB, 2)));
	}

	private Color colorFromString(String color) {
		if (color == null) {
			color = "0, 0, 0";
		}
		String[] colorParams = color.replaceAll("\\s", "").split(",");
		if (colorParams.length != 3) {
			colorParams = new String[] { "0", "0", "0" };
		}

		return new Color(
				Integer.parseInt(colorParams[0]),
				Integer.parseInt(colorParams[1]),
				Integer.parseInt(colorParams[2].replace("*", ""))
		);
	}
}
