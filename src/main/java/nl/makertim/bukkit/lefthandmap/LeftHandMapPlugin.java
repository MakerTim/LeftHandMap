package nl.makertim.bukkit.lefthandmap;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class LeftHandMapPlugin extends JavaPlugin {

	public static LeftHandMapPlugin instance;
	public final static Gson gson = new GsonBuilder()
			.setPrettyPrinting()
			.create();

	public File rootDir;
	public File configFile;

	public Web web = new Web();
	public LeftHandMapConfig config;
	public List<ChunkFinder> finders = new LinkedList<>();


	@Override
	public void onEnable() {
		instance = this;
		this.rootDir = new File(this.getDataFolder(), "web");
		this.rootDir.mkdirs();
		Bukkit.getWorlds().forEach(world -> new File(this.rootDir, world.getName()).mkdir());

		this.configFile = new File(this.getDataFolder(), "config.json");
		try {
			if (!this.configFile.exists()) {
				this.configFile.createNewFile();
				try (PrintWriter out = new PrintWriter(this.configFile)) {
					out.println(gson.toJson(new LeftHandMapConfig()));
					out.flush();
				}
			}
			this.config = gson.fromJson(new FileReader(this.configFile), LeftHandMapConfig.class);

			try (PrintWriter out = new PrintWriter(new File(this.rootDir, "worlds.json"))) {
				out.println(gson.toJson(rootDir.list((dir, name) -> name != null && !name.contains("."))));
				out.flush();
			}
		} catch (Exception ex) {
			System.err.println("Failed to read/write config.json " + ex);
		}

		Bukkit.getWorlds().forEach(world -> {
			if (Arrays.asList(config.worlds).contains(world.getName())) {
				Bukkit.getPluginManager().registerEvents(new ChunkFinder(this, world), this);
			}
		});

		web.onEnable();
	}

	@Override
	public void onDisable() {
		web.onDisable();
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (command.getName().equalsIgnoreCase("maprender")) {
			if (args.length == 0) {
				if (sender instanceof Player && sender.hasPermission("maprender")) {
					World world = ((Player) sender).getWorld();
					ChunkFinder finder = new ChunkFinder(this, world);
					finder.renderWorld();
				} else {
					sender.sendMessage("You have to be in a world to use this command!");
				}
				return true;
			} else {
				if (args[0].equalsIgnoreCase("queue")) {
					sender.sendMessage("QUEUEs: ");
					for (ChunkFinder finder : finders) {
						int individualChunkCounter = finder.chunkQueue.stream().mapToInt(chunkArray -> chunkArray.getList().size()).sum();
						sender.sendMessage("  - " + finder.world.getName() + ": " + finder.chunkQueue.size() +
								" arrays, containing " + individualChunkCounter + " chunks");
					}
				} else if (args[0].equalsIgnoreCase("stop")) {
					for (ChunkFinder finder : finders) {
						finder.chunkQueue.clear();
					}
				} else {
					return false;
				}
				return true;
			}
		}
		return super.onCommand(sender, command, label, args);
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
		if (command.getName().equalsIgnoreCase("maprender")) {
			if (args.length == 0 || args.length == 1) {
				return Arrays.asList("queue", "stop");
			}
		}
		return super.onTabComplete(sender, command, alias, args);
	}
}
