package nl.makertim.bukkit.lefthandmap;

import org.bukkit.ChunkSnapshot;

import java.util.LinkedList;
import java.util.List;

public class ChunkSnapshotArray {

	private int xRegion;
	private int zRegion;
	private List<ChunkSnapshot> list;

	public ChunkSnapshotArray(int xRegion, int zRegion) {
		this.xRegion = xRegion;
		this.zRegion = zRegion;
	}

	public ChunkSnapshotArray(ChunkSnapshot chunk) {
		this.list = new LinkedList<ChunkSnapshot>() {{
			add(chunk);
		}};

		this.xRegion = calculateX(chunk);
		this.zRegion = calculateZ(chunk);
	}

	private int calculateX(ChunkSnapshot chunk) {
		return (int) Math.floor(chunk.getX() / (double) IChunkRenderer.REGION_SIZE);
	}

	private int calculateZ(ChunkSnapshot chunk) {
		return (int) Math.floor(chunk.getZ() / (double) IChunkRenderer.REGION_SIZE);
	}

	public List<ChunkSnapshot> getList() {
		return list;
	}

	public int getXRegion() {
		return xRegion;
	}

	public int getZRegion() {
		return zRegion;
	}

	public void addChunk(ChunkSnapshot chunk) {
		list.removeIf(otherChunk ->
				otherChunk.getX() == chunk.getX() && otherChunk.getZ() == chunk.getZ()
		);
		list.add(chunk);
	}

	public boolean sameRegion(ChunkSnapshot chunk) {
		return sameRegion(calculateX(chunk), calculateZ(chunk));
	}

	public boolean sameRegion(int xRegion, int yRegion) {
		return this.xRegion == xRegion && this.zRegion == yRegion;
	}
}
