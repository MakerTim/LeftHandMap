package nl.makertim.bukkit.lefthandmap;

import spark.Service;

import java.io.File;

public class Web {

	private Service service;

	public void onEnable() {
		service = Service.ignite();

		Mover.exportResource("index.html", new File(LeftHandMapPlugin.instance.rootDir, "index.html"));

		service.port(LeftHandMapPlugin.instance.config.port);
		service.externalStaticFileLocation(LeftHandMapPlugin.instance.rootDir.getPath());
		service.init();
	}

	public void onDisable() {
		service.stop();
	}
}
