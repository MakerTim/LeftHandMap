package nl.makertim.bukkit.lefthandmap;

import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.ChunkSnapshot;
import org.bukkit.World;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.world.ChunkLoadEvent;
import org.bukkit.event.world.ChunkUnloadEvent;
import org.bukkit.scheduler.BukkitTask;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.PrintWriter;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class ChunkFinder implements Listener {

	private LeftHandMapPlugin plugin;
	public final World world;
	private File imgDir;
	private File regionLocation;

	private IChunkRenderer renderer;

	public ConcurrentLinkedQueue<ChunkSnapshotArray> chunkQueue = new ConcurrentLinkedQueue<>();

	public ChunkFinder(LeftHandMapPlugin plugin, World world) {
		this.plugin = plugin;
		this.plugin.finders.add(this);
		this.world = world;
		this.imgDir = new File(plugin.rootDir, world.getName());

		this.regionLocation = new File(world.getWorldFolder(), "region");
		regionLocation.mkdir();

		try {
			renderer = (IChunkRenderer) Class.forName(this.plugin.config.rendererClass).newInstance();
		} catch (Exception ex) {
			System.err.println("CONFIG ERROR, class not found!");
			ex.printStackTrace();
		}

		Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin, this::renderSpawn, 20, 60 * 20);
		Bukkit.getScheduler().runTaskAsynchronously(plugin, this::renderThread);
	}

	public void renderWorld() {
		File[] chunks = regionLocation.listFiles(fileName -> fileName != null && fileName.getName().endsWith(".mca"));
		assert chunks != null;
		for (File file : chunks) {
			Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
				String[] regionData = file.getName().split("\\.");
				int regionX = Integer.parseInt(regionData[1]);
				int regionZ = Integer.parseInt(regionData[2]);

				onRenderRegion(regionX, regionZ);
			});
		}
	}

	public void onRenderRegion(int regionX, int regionZ) {
		ChunkSnapshotArray chunkSnapshotArray = getChunkSnapshotArray(
				queued -> queued.sameRegion(regionX, regionZ),
				() -> new ChunkSnapshotArray(regionX, regionZ)
		);

		final int regionSize = IChunkRenderer.REGION_SIZE;
		for (int relativeChunkX = 0; relativeChunkX < regionSize; relativeChunkX++) {
			for (int relativeChunkZ = 0; relativeChunkZ < regionSize; relativeChunkZ++) {
				chunkSnapshotArray.addChunk(
						world.getChunkAt(((regionX * regionSize) + relativeChunkX), ((regionZ * regionSize) + relativeChunkZ))
								.getChunkSnapshot(true, true, true)
				);
			}
		}
	}

	public void onRenderChunk(ChunkSnapshotArray chunk) {
		int regionX = chunk.getXRegion();
		int regionZ = chunk.getZRegion();
		if (renderer != null) {
			File saveFileLocation = new File(imgDir, String.format("i.%d.%d.png", regionX, regionZ));
			try {
				BufferedImage image = renderer.render(chunk, saveFileLocation);
				ImageIO.write(image, "png", saveFileLocation);
			} catch (Exception ex) {
				System.out.println("Didn't save file " + ex + " @ " + saveFileLocation.getAbsolutePath());
			}
		}
	}

	public void updateJSON() {
		try {
			File saveFileLocation = new File(imgDir, "regions.json");
			try (PrintWriter out = new PrintWriter(saveFileLocation)) {
				out.println(LeftHandMapPlugin.gson.toJson(imgDir.list((dir, name) -> name != null && name.endsWith(".png"))));
				out.flush();
			}
		} catch (Exception ex) {
			System.err.println("Failed to save regions.json " + ex);
		}
	}

	public void renderSpawn() {
		int xOffset = world.getSpawnLocation().getChunk().getX();
		int zOffset = world.getSpawnLocation().getChunk().getZ();
		int renderDistance = Bukkit.getViewDistance() + 2;
		for (int x = -1 * renderDistance; x <= renderDistance; x++) {
			for (int z = -1 * renderDistance; z <= renderDistance; z++) {
				addToQueue(world.getChunkAt(x + xOffset, z + zOffset));
			}
		}
	}

	public void renderThread(BukkitTask task) {
		while (!task.isCancelled()) {
			try {
				ChunkSnapshotArray snapshot = this.chunkQueue.poll();
				if (snapshot != null) {
					synchronized (world) {
						onRenderChunk(snapshot);
						updateJSON();
					}
				} else {
					Thread.sleep(500);
				}
			} catch (Exception ex) {
				System.err.println("RENDER THREAD EXCEPTION");
				ex.printStackTrace();
			}
		}
	}

	private ChunkSnapshotArray getChunkSnapshotArray(Predicate<ChunkSnapshotArray> filter, Supplier<ChunkSnapshotArray> newInstance) {
		return chunkQueue.stream()
				.filter(filter)
				.findFirst()
				.orElseGet(() -> {
					ChunkSnapshotArray instance = newInstance.get();
					chunkQueue.add(instance);
					return instance;
				});
	}

	private void addToQueue(Chunk chunk) {
		addSnapshotToQueue(chunk.getChunkSnapshot(true, true, true));
	}

	private void addSnapshotToQueue(ChunkSnapshot chunkSnapshot) {
		ChunkSnapshotArray chunkSnapshotArray = getChunkSnapshotArray(
				queued -> queued.sameRegion(chunkSnapshot),
				() -> new ChunkSnapshotArray(chunkSnapshot)
		);
		chunkSnapshotArray.addChunk(chunkSnapshot);
	}

	@EventHandler
	public void onChunkLoad(ChunkLoadEvent event) {
		if (event.getWorld() == world) {
			addToQueue(event.getChunk());
		}
	}

	@EventHandler
	public void onChunkUnLoad(ChunkUnloadEvent event) {
		if (event.getWorld() == world) {
			addToQueue(event.getChunk());
		}
	}
}
